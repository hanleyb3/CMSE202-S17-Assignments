import matplotlib.pyplot as plt
import random 
import numpy as np

mutation_rate = 0.2

class bug():
    '''Bug class. Each bug has n=3 genese of values 0-1 and a location in the world (c,r)'''

    
    def __init__(self, c=0,r=0):
        self.genes = [0.5,0.5,0.5]
        self.loc = (0,0)
        '''inicialize a bug with random genes. Default location is 0,0.'''
        self.genes = [random.random(), random.random(), random.random()]
        self.loc = (c,r)
        pass
    
    def mitosis(self):
        '''Make a copy of the current bug with a probablity of mutation_rate that each gene will randomly mutate.'''
        newbug = bug()
        for i,g in enumerate(self.genes):
            if random.random() > mutation_rate:
                newbug.genes[i] = self.genes[i]
        return newbug
    
    def draw(self):
        '''Draw the bug with rgb color corresponding to genes'''
        plt.scatter(self.loc[0], self.loc[1], color=self.genes)

class petri_dish():
    '''Generate a petridish representing the world where the bugs live. Each dish has antibotics and a list of bugs.'''

    
    def __init__(self):
        '''Inicializes the world. Default rows=45 and cols-90.  The left most and right most columns are
        inicialized with random bugs.'''
        
        self.antibodies = np.zeros((1,1,3))
        self.buglist = dict()
        self.active = set()
        self.basic_setup()
    
    def basic_setup(self, n_rows = 45, n_cols = 90):
        for row in range(n_rows):
            br = bug(0,row)
            bl = bug(n_cols-1,row)
            
            #Add inicial points to active and buglist
            self.buglist[br.loc] = br
            self.buglist[bl.loc] = bl
            self.active.add(br.loc)
            self.active.add(bl.loc)
            
        ##Set up board##
        self.antibodies=np.zeros((n_rows,n_cols,3))
        self.antibodies[:,10:80] = [0.5,0.5,0.5]
        self.antibodies[:,20:70] = [0.75,0.75,0.75]
        self.antibodies[:,30:60] = [0.8,0.8,0.8]
        self.antibodies[:,40:50] = [0.95,0.95,0.95]
        
    def timestep(self):        
        '''Loop threw the bugs, find their neighbors and iniciate mitosis.  New bugs will 
        die if their gene values are less than the corresponding antibiotic values.'''
        #loop though bugs in population
        newbugs = dict()
        n_cols = self.antibodies.shape[1]
        n_rows = self.antibodies.shape[0]
        
        these_bugs = self.active.copy()
        
        for b in these_bugs:
            c = b[0]
            r = b[1]
            parent_bug = self.buglist[b]
            
            neighbors = []
            for dc in range(c-1,c+2):
                for dr in range(r-1,r+2):
                    key = (dc,dr)
                    if dc >= 0 and dc < n_cols and dr >= 0 and dr < n_rows:
                        if not key in self.buglist:
                            neighbors.append(key)
            if neighbors:
                child_bug = parent_bug.mitosis()
                loc = random.choice(neighbors)
                
                #Check if bug lives
                child_bug.loc = loc
                alive = True
                for i,g in enumerate(child_bug.genes):
                    if g < self.antibodies[loc[1], loc[0],i]:
                        alive = False
                if alive:
                    newbugs[child_bug.loc] = child_bug
            else:
                self.active.remove((c,r))
                
        self.buglist.update(newbugs)
        self.active.update(newbugs)
                
    def draw(self):
        '''Draw the world as an image and plot each bug.'''
        plt.imshow(self.antibodies)
        for b in self.buglist:
            self.buglist[b].draw()
    